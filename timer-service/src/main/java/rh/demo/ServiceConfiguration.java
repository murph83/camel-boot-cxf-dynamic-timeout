package rh.demo;

import org.apache.camel.CamelContext;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.cxf.CxfEndpoint;
import org.apache.camel.component.cxf.DataFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

/**
 * CXF Configuration
 */
@Configuration
public class ServiceConfiguration  extends RouteBuilder{

    /**
     * Wire in the Camel Context
     */
    @Autowired
    CamelContext context;

    /**
     * Configure the CXF Endpoint, and a simple route that delays for X milliseconds before returning a success message
     * @throws Exception in the event of mis-configuration
     */
    public void configure() throws Exception {
        CxfEndpoint cxfEndpoint = new CxfEndpoint();
        cxfEndpoint.setAddress("http://localhost:8088/timer");
        cxfEndpoint.setServiceClass(TimerService.class);
        cxfEndpoint.setCamelContext(context);
        cxfEndpoint.setDataFormat(DataFormat.POJO);

        context.addEndpoint("timerEndpoint", cxfEndpoint);

        from(cxfEndpoint).delay(simple("${body}")).setBody(constant("Execution Complete"));

    }
}
