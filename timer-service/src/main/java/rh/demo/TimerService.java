package rh.demo;

/**
 * TimerService defines a contract for a service that will wait for a period of time before returning a success message
 */
public interface TimerService {

    /**
     * Wait for a period of time before returning a success message
     * @param milllis Time to wait in Milliseconds
     * @return Success Message
     */
    String wait(int milllis);
}
