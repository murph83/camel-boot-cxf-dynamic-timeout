#!/bin/sh

# Display progress ticker
while :; do
    printf "."
    sleep .2
done &
tickerid=$!

# Run the test
RESPONSE=$(curl -s -d 1500 --header 'Content-Type: application/json' http://localhost:9080/rest/test)

# Kill ticker
kill $tickerid
wait $tickerid 2>/dev/null

# Check for the timeout exception
if echo $RESPONSE | grep -q "java.net.SocketTimeoutException: Read timed out"; then
    echo "Test success"
else
    echo "Test failure"
fi
