package rh.demo.rest;

import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.rest.RestBindingMode;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Demonstrates the use of the Java DSL for configuring the REST components.
 * Utilizes camel-syntax property replacement from application.properties without the use of a Bridge,
 * since Spring Boot already includes the appropriate property resolvers
 */
@Configuration
public class RestConfiguration extends RouteBuilder {

    @Override
    public void configure() throws Exception {

        restConfiguration()
                .apiContextPath("/api-docs")
                .bindingMode(RestBindingMode.off)
                .enableCORS(true)
                .scheme("http")
                .host("{{rest.configuration.host}}")
                .port("{{rest.configuration.port}}")
                .contextPath("{{rest.configuration.contextPath}}")
                .dataFormatProperty("prettyPrint", "true")
                .apiProperty("host", "{{rest.configuration.host}}")
                .apiProperty("api.version", "1.0.0")
                .apiProperty("api.title", "TITLE")
                .apiProperty("api.description", "DESCRIPTION")
                .apiProperty("api.contact.name", "NAME");

    }

}
