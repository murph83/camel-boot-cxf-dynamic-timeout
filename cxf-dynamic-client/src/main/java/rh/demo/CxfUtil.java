package rh.demo;

import org.apache.camel.Exchange;
import org.apache.camel.component.cxf.CxfPayload;
import org.apache.camel.converter.jaxp.XmlConverter;
import org.apache.cxf.binding.soap.SoapHeader;
import org.apache.cxf.endpoint.Client;
import org.apache.cxf.transports.http.configuration.HTTPClientPolicy;
import org.springframework.stereotype.Component;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.*;

/**
 * Bean Component to provide helpers for CXF client requests
 */
@Component("cxfUtil")
public class CxfUtil {

    /**
     * Set a specific request timeout on the Exchange
     * Use:
     * <to uri="bean:cxfUtil?method=setTimeout(*, 3000)"/>
     * @param exchange Exchange to set the request context
     * @param timeout int milliseconds
     * @param  timeoutOffset int offset to add to the timeout
     */
    public void setTimeout(Exchange exchange, int timeout, int timeoutOffset) {
        // Create ClientPolicy
        HTTPClientPolicy clientPolicy = new HTTPClientPolicy();
        clientPolicy.setReceiveTimeout(timeout + timeoutOffset);

        // Populate RequestContext with the Policy and set it on the exchange
        Map<String, Object> requestContext = new HashMap<>();
        requestContext.put(HTTPClientPolicy.class.getName(), clientPolicy);
        exchange.setProperty(Client.REQUEST_CONTEXT, requestContext);
    }

    /**
     * Create a CxfPayload for the {demo.rh}/wait service endpoint with the given wait time.
     * @param exchange Exchange to set the CxfPayload
     * @param timeout int milliseconds to wait
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws IOException
     */
    public void createPayload(Exchange exchange, int timeout) throws ParserConfigurationException, SAXException, IOException {
        List<Element> outElements = new ArrayList<>();
        XmlConverter converter = new XmlConverter();
        Document outDocument = converter.toDOMDocument("<demo:wait xmlns:demo=\"http://demo.rh/\"><demo:arg0>" + timeout + "</demo:arg0></demo:wait>", exchange);
        outElements.add(outDocument.getDocumentElement());
        CxfPayload<SoapHeader> responsePayload = new CxfPayload<>(Collections.emptyList(), outElements);
        exchange.getIn().setBody(responsePayload);
    }
}
