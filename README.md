Setting CXF Client Timeouts Dynamically in Camel
======================

This project demonstrates how to dynamically set the ReceiveTimeout on a camel-cxf component at runtime.

It contains two projects

timer-service
------------------------
This project implements a *very* simple SOAP service, as defined by [TimerService.java](timer-service/src/main/java/rh/demo/TimerService.java). It will wait for the input number of milliseconds before returning a success message. This allows us to play around with expected timeouts.
 

cxf-dynamic-client
-----------------------------
This project demonstrates the use of CXF Request Context to override the default receive timeout on a per-request basis.

In particular, [CxfUtil.java](cxf-dynamic-client/src/main/java/rh/demo/CxfUtil.java) has a method `setTimeout` that performs the setting. 
```java
public void setTimeout(Exchange exchange, int timeout, int timeoutOffset) {
        // Create ClientPolicy
        HTTPClientPolicy clientPolicy = new HTTPClientPolicy();
        clientPolicy.setReceiveTimeout(timeout + timeoutOffset);

        // Populate RequestContext with the Policy and set it on the exchange
        Map<String, Object> requestContext = new HashMap<>();
        requestContext.put(HTTPClientPolicy.class.getName(), clientPolicy);
        exchange.setProperty(Client.REQUEST_CONTEXT, requestContext);
    }
```

Testing
-------
In the root directory, there is a script `start.sh` that will start the timer-service and cxf-dynamic-client spring boot applications. 

Then, you can invoke `test.sh` to start the test. The flow looks like this:

![test flow](testflow.png)

The test script expects to see the timeout exception, and will report success only if it is present. You can review the full output of the services in the `*.log` files created in the root directory when the services start.

Once the test is complete, you may run `stop.sh` to terminate the services.
