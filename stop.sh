#!/bin/sh

# Stop everything, but leave logs
echo "Stopping timer-service"
[ -e timer-service.pid ] && pkill -F timer-service.pid && rm timer-service.pid

echo "Stopping cxf-dynamic-client"
[ -e cxf-dynamic-client.pid ] && pkill -F cxf-dynamic-client.pid && rm cxf-dynamic-client.pid
