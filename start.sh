#!/bin/sh
pushd () {
    command pushd "$@" > /dev/null
}

popd () {
    command popd "$@" > /dev/null
}


# Clean up
[ -e timer-service.pid ] && pkill -F timer-service.pid
[ -e cxf-dynamic-client.pid ] && pkill -F cxf-dynamic-client.pid
rm -f timer-service.log cxf-dynamic-client.log build.log

# Build
echo "Building project, see build.log for output"
mvn clean install > build.log 2>&1

# Run
echo "Starting timer-service, see timer-service.log for output"
pushd timer-service
nohup mvn spring-boot:run > ../timer-service.log 2>&1 & echo $! > ../timer-service.pid
popd

sleep 1

echo "Starting cxf-dynamic-client, see cxf-dynamic-client.log for output"
pushd cxf-dynamic-client
nohup mvn spring-boot:run > ../cxf-dynamic-client.log 2>&1 & echo $! > ../cxf-dynamic-client.pid
popd
